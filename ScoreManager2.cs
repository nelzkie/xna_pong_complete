﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public enum StartPosition
    {
        Beginning,
        Ending
    }
    public class ScoreManager2 : GameEntity
    {


        public int score = 0;
        public Vector2 scorePosition = Vector2.Zero;
        public StartPosition StartPosition;
        public Ball ball { get; set; }
        public string msg = "oh cmon";
        private int counter = 0;
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        private SpriteFont scorefont;
        public bool transitionDone;
        public bool scoretrigger;
        public float wallCollision = 0;
        public override void Activate(ScreenStateManager screenStateManager, bool b)
        {
            base.Activate(screenStateManager, b);
            speed = 200f;
            scorefont = content.Load<SpriteFont>("scorefont");
           // direction.X = 1;
            
        }

        public void Reset()
        {
            
            if (StartPosition == StartPosition.Beginning)
            {
               
                position.X = -scorefont.MeasureString(msg).X;
            }
            if (StartPosition == StartPosition.Ending)
            {
                
                position.X = viewportBounds.Width;
            }
           
        }

        public void PlayerServe()
        {
            
            
            if (StartPosition == StartPosition.Beginning) // if we start left side of the viewport bounds(0 x position)
            {
                if (position.X > wallCollision && !scoretrigger)  // then change the condition cause it will move toward  position x position
                {
                   
                    scoretrigger = true;
                    transitionDone = true;
                }
            }
            if (StartPosition == StartPosition.Ending) // if we start and the edge of viewport bounds
            {
                if (position.X < wallCollision && !scoretrigger)    // then change the condition cause it will move toward -x position
                {
                    
                    scoretrigger = true;    // this will be set to false when transitioning is done in Ball class
                    transitionDone = true;
                }
            }
            if (transitionDone)
            {
               
                Reset();
            }
        }



        public float getHeight()
        {
            return scorefont.LineSpacing;
        }



        public override void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {

            
            position += ((direction * (float)gameTime.ElapsedGameTime.TotalSeconds) * speed); 
           
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.DrawString(scorefont,score.ToString(),scorePosition,Color.Red);
            spriteBatch.DrawString(scorefont, msg, position, Color.Red);
        }
    }
}
