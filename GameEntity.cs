﻿using System;
using System.Diagnostics;
using System.Security;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public abstract class GameEntity
    {
     
        public ScreenStateManager ScreenStateManager { get; set; }
        public Rectangle bounds;
        protected Texture2D texture;
        private Texture2D background, netBackground;
        protected ContentManager content;
        protected SpriteBatch spriteBatch;
        public Vector2 position, direction;
        protected Collision collision = new Collision();
        public Rectangle viewportBounds;
        protected Sounds sounds;
        public float speed { get; set; }


        public virtual void Activate(ScreenStateManager screenStateManager, bool b)
        {
            if (b)
            {
                this.ScreenStateManager = screenStateManager;
                content = new ContentManager(ScreenStateManager.Game.Services, "Content");

            }
            spriteBatch = ScreenStateManager.SpriteBatch;
            viewportBounds = ScreenStateManager.GraphicsDevice.Viewport.Bounds;
            background = content.Load<Texture2D>("background1");
            netBackground = content.Load<Texture2D>("backgroundMiddle");
        }


        public Vector2 getTextureSize()
        {
            return new Vector2(texture.Width,texture.Height);
        }

        /// <summary>
        /// Set the rectangle bounds for the entity
        /// </summary>
        /// <param name="texture"></param>
        public void setBounds(Texture2D texture)
        {
            bounds = new Rectangle((int)position.X,(int)position.Y,texture.Width,texture.Height);
        }

  

        public virtual  void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            bounds = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            Vector2 tmpPos = position + ((direction * (float)gameTime.ElapsedGameTime.TotalSeconds) * speed); ;
            if (collision.BoundsCollide(ref tmpPos, texture, ref viewportBounds))
            {
                
                return;
            }
            
         
            position = tmpPos;

        }
        public virtual void Draw(GameTime gameTime)
        {
            
           
           
            spriteBatch.Draw(texture, position, Color.White);
            
        }
    }
}