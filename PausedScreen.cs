﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class PausedScreen : IScreenState
    {
        public ScreenStateManager ScreenStateManager { get; set; }
        private MenuEntry resumeEntry, quitEntry;
        private SpriteBatch spriteBatch;
        private ContentManager content;
        private Texture2D blankTexture;
        List<MenuEntry>menuEntries = new List<MenuEntry>();
        private int selectedEntry = 0;
        public PausedScreen()
        {
            resumeEntry = new MenuEntry("Resume");
            quitEntry = new MenuEntry("Quit");

            resumeEntry.SelectInputHandler += ResumeEntry_SelectInputHandler;
            quitEntry.SelectInputHandler += QuitEntry_SelectInputHandler;
            menuEntries.Add(resumeEntry);
            menuEntries.Add(quitEntry);
        }

        private void QuitEntry_SelectInputHandler(object sender, EventArgs e)
        {
            ScreenStateManager.escCounter++;    // increase the pause counter
            ScreenStateManager.gameplayState = ScreenState.Quit;
            ScreenStateManager.RemoveScreen(this);
            ScreenStateManager.Add(new MenuScreen());
        }

        private void ResumeEntry_SelectInputHandler(object sender, EventArgs e)
        {
            ScreenStateManager.escCounter++;    // increase the pause counter
            ScreenStateManager.gameplayState = ScreenState.Resume;
            ScreenStateManager.RemoveScreen(this);
        }

        public void Draw(GameTime gameTime)
        {
            Vector2 position = new Vector2(100, 100);
            for (int i = 0; i < menuEntries.Count; i++)
            {
                position.X = 300;
               
                position.Y += menuEntries[i].getHeight(this);
                menuEntries[i].Position = position;
                menuEntries[i].Draw(gameTime, i == selectedEntry, this);
            }
            spriteBatch.Draw(blankTexture,ScreenStateManager.GraphicsDevice.Viewport.Bounds,Color.Black * 0.2f);
        }

        public void Activate(bool b)
        {
            if (b)
            {
                spriteBatch = ScreenStateManager.SpriteBatch;
                content = new ContentManager(ScreenStateManager.Game.Services, "Content");
            }
            blankTexture = content.Load<Texture2D>("blank");
        }

        public void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            if (state.IsKeyDown(Keys.Up) && oldstate.IsKeyUp(Keys.Up))
            {
                selectedEntry--;
                ScreenStateManager.Sounds.SelectPlay(); // play the select sound
                if (selectedEntry < 0) selectedEntry = 0;
            }
            if (state.IsKeyDown(Keys.Down) && oldstate.IsKeyUp(Keys.Down))
            {
                selectedEntry++;
                ScreenStateManager.Sounds.SelectPlay(); // play the select sound
                if (selectedEntry > menuEntries.Count - 1) selectedEntry = menuEntries.Count - 1;
            }


            if (state.IsKeyDown(Keys.Escape) && oldstate.IsKeyUp(Keys.Escape))  // NOTE that GamePlayManager class is still listening to the Input even though PauseScreen is on screen
            {
                Debug.WriteLine("remove");

                if (ScreenStateManager.gameplayState == ScreenState.Pause)
                {
                    ScreenStateManager.gameplayState = ScreenState.Resume;
                    ScreenStateManager.RemoveScreen(this);
                  
                }
             

            }
            for (int i = 0; i < menuEntries.Count; i++)
            {

                menuEntries[i].Update(gameTime, i == selectedEntry);
            }

            if (state.IsKeyDown(Keys.Enter) && oldstate.IsKeyUp(Keys.Enter))
            {
                ScreenStateManager.Sounds.BackPlay();
                menuEntries[selectedEntry].OnSelectedEntry();
            }
        }
    }
}
