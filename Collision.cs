﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace My_Own_Menu_State_System_1
{
    public class Collision
    {
        /// <summary>
        /// viewport collision detection
        /// </summary>
        /// <param name="position"> temporary entity position generated from update method</param>
        /// <param name="texture">the sprite texture</param>
        /// <param name="bounds"> viewport</param>
        /// <returns></returns>
        public bool BoundsCollide(ref Vector2 position, Texture2D texture, ref Rectangle bounds)
        {
            
            if (position.Y < 0 || position.Y + texture.Height > bounds.Height)
            {
                
                return true;
            }
            return false;
        }

        public Vector2 BallViewportCollision(ref Vector2 position, ref Vector2 direction, Texture2D texture, ref Rectangle viewportBounds)
        {
            Vector2 tmpVector = direction;
            if (position.Y > viewportBounds.Height - texture.Height || position.Y < 0)
            {
                tmpVector.Y *= -1;
            }            
            return tmpVector;
        }
        
    }
}
