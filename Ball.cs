﻿using System;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization.Formatters;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class Ball : GameEntity
    {
        private Random random  = new Random();
        public GameEntity player { get; set; }
        public AiEnemyPlayer Ai { get; set; }
        //public ScoreManager ScoreManager { get; set; }
        private bool playerServe, enemyServe;
        public bool AITrigerMovement = false;
        private bool startgame = true;
        public Winner winner = Winner.Both;
        public bool isBounce;
        private ScoreManager2 enemyScoreManager2, playerScoreManager2;
        public Ball()
        {
            position = new Vector2(300,100);
            speed = 100f;
            direction = new Vector2((random.Next(0, 2) * 2) - 1, random.Next(0, 2) * 2 - 1);  // random.next(0,2) will select number between 0,1. 2 is not included 
            //direction = new Vector2(-1,0);
        }


        /// <summary>
        /// The reason we need to define the content here on this function is that when we declare it on constructor the ScreenStateManager will be null because the constructor is called first 
        /// when we add the Ball class to the List of ScreenStatemanager in the Game class
        /// </summary>
        /// <param name="stateManager"></param>
        /// <param name="screenStateManager"></param>
        /// <param name="b"></param>
        public override void Activate( ScreenStateManager screenStateManager, bool b)
        {
            base.Activate(screenStateManager,b);
            texture = content.Load<Texture2D>("ball");

            // setting up the ScoreManager for enemy since this is only exclusive for Ball class
            enemyScoreManager2 = new ScoreManager2();
            enemyScoreManager2.direction.X = 1;
            enemyScoreManager2.wallCollision = viewportBounds.Width;
            enemyScoreManager2.StartPosition = StartPosition.Beginning;
            enemyScoreManager2.msg = " Enemy Score! ";
            enemyScoreManager2.scorePosition = new Vector2(viewportBounds.Width / 4,100);
            enemyScoreManager2.Activate(screenStateManager, true);
            enemyScoreManager2.Reset();



            // setting up the ScoreManager for Player since this is only exclusive for Ball class
            playerScoreManager2 = new ScoreManager2();
            playerScoreManager2.direction.X = -1;
            playerScoreManager2.wallCollision = 0;
            playerScoreManager2.StartPosition = StartPosition.Ending;
            playerScoreManager2.msg = " Player Score! ";
            playerScoreManager2.scorePosition = new Vector2(viewportBounds.Width * 3/4 , 100);  // this viewportBounds.Width * 3/4  is to get the half of the right half side of screen width (yeah ikr, CONFUSING!!!)
            playerScoreManager2.Activate(screenStateManager, true);
            playerScoreManager2.position.Y = viewportBounds.Height - playerScoreManager2.getHeight();
            playerScoreManager2.Reset();

        }

        /// <summary>
        /// Collision between the paddle and the ball
        /// </summary>
        public void Collision()
        {
            bounds = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
           
            if (bounds.Intersects(player.bounds))
            {
                ScreenStateManager.Sounds.Wallbounce(); // play sounds 
                position.X = player.position.X + player.getTextureSize().X;
                //speed = 20f;
                direction.X *= -1;  // since the ball is just moving on the x direction, we just reflect the x direction
            }
            if ( bounds.Intersects(Ai.bounds))
            {
                ScreenStateManager.Sounds.Wallbounce(); // play sounds 
                position.X = Ai.position.X - Ai.getTextureSize().X;
                //speed = 20f;
                isBounce = true;
                direction.X *= -1;  // since the ball is just moving on the x direction, we just reflect the x direction
            }

        }

        //public void PlayerServe()
        //{
        //    if (ScoreManager.enemyScore && !ScoreManager.transitionDone )   // while the transition of score text is not done, we can still move the paddle together with ball
        //    {
        //        direction = Vector2.Zero;   // disable the location where the ball is going so that it wont move if we dont press the space bar
        //        //position.X = player.position.X + player.getTextureSize().X;
        //        //position.Y = player.position.Y + ((player.getTextureSize().Y / 2) - (texture.Height / 2));

        //        startgame = false;  // this is our trigger that our game has started already so we set this to false
        //        playerServe = true;
        //    }
        //    if (!startgame && playerServe)
        //    {
        //        position.X = player.position.X + player.getTextureSize().X; // set the position of the ball in relation to our paddle
        //        position.Y = player.position.Y + ((player.getTextureSize().Y / 2) - (texture.Height / 2)); // set the position of the ball in relation to our paddle
        //    }
        //}

        //public void EnemyServe()
        //{
        //    if (ScoreManager.playerScore && !ScoreManager.transitionDone)
        //    {
        //        direction = Vector2.Zero;
        //        position.X = Ai.position.X + Ai.getTextureSize().X;
        //        position.Y = Ai.position.Y + ((Ai.getTextureSize().Y / 2) - (texture.Height / 2));

        //        startgame = false;
        //        enemyServe = true;
        //    }
        //    if (!startgame && enemyServe)
        //    {
        //        position.X = Ai.position.X + Ai.getTextureSize().X;
        //        position.Y = Ai.position.Y + ((Ai.getTextureSize().Y / 2) - (texture.Height / 2));
        //    }
        //}

        private void PlayerServe(GameTime gameTime,KeyboardState state, KeyboardState oldstate)
        {
            if (position.X < 0)
            {
                ScreenStateManager.Sounds.ScorePlay();
                playerServe = true;
                playerScoreManager2.score++;

            }
            if (playerServe)
            {
                enemyScoreManager2.PlayerServe();
                enemyScoreManager2.Update(gameTime, state, oldstate);
                direction = Vector2.Zero;
                position.X = player.position.X + player.getTextureSize().X; // set the position of the ball in relation to our paddle
                position.Y = player.position.Y + ((player.getTextureSize().Y / 2) - (texture.Height / 2)); // set the position of the ball in relation to our paddle
            }



            if (state.IsKeyDown(Keys.Space) && oldstate.IsKeyUp(Keys.Space) && playerServe)
            {

                if (enemyScoreManager2.transitionDone)  // the score msg is done with transitioning
                {
                    direction.X = 1;    
                    direction.Y = random.Next(0, 2) * 2 - 1;;
                    playerServe = false;
                    enemyScoreManager2.scoretrigger = false;
                    enemyScoreManager2.transitionDone = false;
                }
            }
        }

        public void EnemyServe(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            if (position.X > viewportBounds.Width)
            {
                ScreenStateManager.Sounds.ScorePlay();
                enemyServe = true;
                enemyScoreManager2.score++;
                AITrigerMovement = true;
                direction = Vector2.Zero;

            }
            if (enemyServe)
            {

                playerScoreManager2.PlayerServe();
                playerScoreManager2.Update(gameTime, state, oldstate);

                position.X = Ai.position.X - Ai.getTextureSize().X; // set the position of the ball in relation to our paddle
                position.Y = Ai.position.Y + ((Ai.getTextureSize().Y / 2) - (texture.Height / 2)); // set the position of the ball in relation to our paddle


            }
            if (playerScoreManager2.transitionDone && enemyServe)
            {
                direction.X = -1;
                direction.Y = random.Next(0, 2) * 2 - 1; ;
                enemyServe = false;
                playerScoreManager2.scoretrigger = false;
                playerScoreManager2.transitionDone = false;
            }
        }
        public void gameOverCheck() // check gameover
        {
            if (enemyScoreManager2.score == 7)
            {
               
                winner = Winner.Player;
            }
            if (playerScoreManager2.score == 7)
            {
                
                winner = Winner.Enemy;
            }
            
        }
        public override void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            Collision();
            gameOverCheck();
            speed--;
            if (speed < 50f)
            {
                speed = 50f;
            }
            PlayerServe(gameTime,state,oldstate);
            EnemyServe(gameTime, state, oldstate);
            direction = collision.BallViewportCollision(ref position, ref direction, texture, ref viewportBounds);
            position += ((direction * (float)gameTime.ElapsedGameTime.TotalSeconds) * speed);
        }
        public override void Draw(GameTime gameTime)
        {
            enemyScoreManager2.Draw(gameTime);
            playerScoreManager2.Draw(gameTime);
            base.Draw(gameTime);
        }
    }
}
