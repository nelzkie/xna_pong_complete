﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace My_Own_Menu_State_System_1
{
    public class MenuEntry
    {
        private  string _msg;
       
        public string MSG { get { return _msg; } set { _msg = value; } }
        private  Vector2 position;
        public Vector2 Position { get { return position; }  set { position = value; } }
        private SpriteFont font;
        private SpriteBatch spriteBatch;
        private float selectionFade;
        public event EventHandler SelectInputHandler;
        public event EventHandler CancelSelectInputHandler;
        public bool disablePulsate;
        public MenuEntry(string msg)
        {
            _msg = msg;
            
           
        }

        public void Update(GameTime gameTime, bool isSelected)
        {
            float fadeSpeed = (float) gameTime.ElapsedGameTime.TotalSeconds;
            if (isSelected)
            {
                selectionFade = 5;
            }
            else
            {
                selectionFade = Math.Max(0, 0); // normally we make the selection fade slower and slower until it doesnt pulsate but for now 0 is okay when its not selected
            }
        }

        public void Draw(GameTime gameTime, bool isSelected, IScreenState menuScreen)
        {
            font = menuScreen.ScreenStateManager.SpriteFont;
            spriteBatch = menuScreen.ScreenStateManager.SpriteBatch;
            Color color = isSelected ? Color.Green : Color.Yellow;
            float scale = 1;
            if (!disablePulsate)
            {
                

                double time = gameTime.TotalGameTime.TotalSeconds;  // Note: here we use TotalGameTime not ElapseGametime. Look it up on google to see difference


                int speedpulsate = 7; // the speed of pulsate

                //Basically the Math.Cos and/or Math.Sin will return a value between 1 and -1;
                // Never forget to experiment to understand it clearly.
              
                float pulsate = (float)Math.Cos(time * speedpulsate) + 1;   // note: to understand whats the connection of Math.cos here you need to understand the Unit Circle and what it does

                 scale = 1 + pulsate * 0.05f * selectionFade;

            
            }
            spriteBatch.DrawString(font, _msg, position, color, 0, Vector2.Zero, scale, SpriteEffects.None, 0);

        }



        public virtual void OnSelectedEntry()
        {
            if (SelectInputHandler != null)
            {
                SelectInputHandler(this, EventArgs.Empty);
            }
        }
        

        public virtual void OnCancelSelectedEntry()
        {
            if (CancelSelectInputHandler != null)
            {
                CancelSelectInputHandler(this, EventArgs.Empty);
            }
        }
        public int getHeight(IScreenState screen)
        {
            return screen.ScreenStateManager.SpriteFont.LineSpacing;
        }

        public int getWidth(MenuScreen screen)
        {
            return (int) screen.ScreenStateManager.SpriteFont.MeasureString(_msg).X;
        }
    }
}
