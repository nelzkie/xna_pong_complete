﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class GameOver : IScreenState
    {
        private SpriteFont gameoverFont;
        private ContentManager content;
        private SpriteBatch spriteBatch;
        public string msg { get; set; }
        public ScreenStateManager ScreenStateManager { get; set; }



        public GameOver(string msg)
        {
            this.msg = msg + " Wins!";
           
        }
  

        public void Draw(GameTime gameTime)
        {
            ScreenStateManager.GraphicsDevice.Clear(Color.Black);
            spriteBatch.DrawString(gameoverFont, msg , new Vector2(ScreenStateManager.GraphicsDevice.Viewport.Width / 2 - gameoverFont.MeasureString(msg).X / 2, ScreenStateManager.GraphicsDevice.Viewport.Height / 2 - gameoverFont.LineSpacing /2 ), Color.White);
        }

        public void Activate(bool b)
        {
            
            content = new ContentManager(ScreenStateManager.Game.Services, "Content");
            spriteBatch = ScreenStateManager.SpriteBatch;
            gameoverFont = content.Load<SpriteFont>("gameoverfont");
            Debug.WriteLine( gameoverFont.MeasureString(msg).X / 2);
            ScreenStateManager.Sounds.WinPlay();
        }

        public void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            
        }
    }
}
