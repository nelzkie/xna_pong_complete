﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class AiEnemyPlayer : GameEntity
    {
        public Ball ball { get; set; }
        public Winner Winner;
        public override void Activate(ScreenStateManager screenStateManager, bool b)
        {
            base.Activate(screenStateManager, b);
            texture = content.Load<Texture2D>("paddle");
            speed = (ScreenStateManager.dificulty == Level.Hard) ? 350f : 300f;
           
            position = new Vector2(viewportBounds.Width - texture.Width - (texture.Width / 2), 300);

        }

        /// <summary>
        /// The movement of our AI is base on the direction of the ball
        /// </summary>
        /// <param name="gameTime"></param>
        private void AiMovement(GameTime gameTime)
        {

            // This is just a solution found on reddit to have a harder difficulty  AI
            //http://www.reddit.com/r/Unity2D/comments/2u8ydg/trouble_with_pong_enemy_ai/
            float moveDistance = speed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            float ballDistance = Math.Abs((position.Y + texture.Height / 2) - (ball.position.Y + ball.getTextureSize().Y / 2));    // Explanation about math.abs http://xboxforums.create.msdn.com/forums/p/34583/199261.aspx


            //if (moveDistance > ballDistance)
            //{
            //    moveDistance = ballDistance / 2;
            //}

            moveDistance = Math.Abs(ball.direction.Y);

            float aY = position.Y + texture.Height/2 - (ball.getTextureSize().Y / 2);   // get the center of paddle
            
            if (ball.position.X > viewportBounds.Width/2)   // when the ball reach the center of the screen
            {

             
                if (ball.position.Y > aY    && ball.direction.Y == 1)
                {
                    direction = new Vector2(0, 1)*moveDistance;

                }
                else if (ball.position.Y < aY && ball.direction.Y == -1)
                {
                    direction = new Vector2(0, -1)*moveDistance;
                }


                //bounds = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
                //Vector2 tmpPos = position + ((direction * (float)gameTime.ElapsedGameTime.TotalSeconds) * ball.speed); ;
                //if (collision.BoundsCollide(ref tmpPos, texture, ref viewportBounds))
                //{

                //    return;
                //}
                //position = tmpPos;


            }
            else
            {
                //direction.Y = 0;
                if (ScreenStateManager.dificulty == Level.Hard) // set our AI movement base on dificulty level
                {
                    if (ball.direction.Y == -1)
                    {
                        direction.Y = 1;

                    }
                    else if (ball.direction.Y == 1)
                    {
                        direction.Y = -1;
                    }

                    if (position.Y > 300)
                    {
                        direction.Y = 0;
                    }
                }
                else
                {
                    if (ball.position.Y > position.Y && ball.direction.Y == 1)
                    {
                        direction.Y = ball.direction.Y;
                    }
                    if (ball.position.Y < position.Y && ball.direction.Y == -1)
                    {
                        direction.Y = ball.direction.Y;
                    }
                }


            }

        

            if (ball.AITrigerMovement)  // this is for when the enemy is about to serve we move him on the center of the screen
            {
                
                if (position.Y + (texture.Height/2) > viewportBounds.Height/2)
                {
                    direction.Y = -1;
                    
                }
                else
                {
                    ball.AITrigerMovement = false;
                    direction.Y = 0;
                }
            }
        }

     

        public override void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            base.Update(gameTime, state, oldstate);
            AiMovement(gameTime);
            


        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
