﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{

    public enum Winner
    {
        Player,
        Enemy,
        Both
    }
    public class GamePlayManager : IScreenState
    {
        public Vector2 Position;
        public Rectangle bounds;
        private Texture2D texture;

        private SpriteBatch spriteBatch;
        private Ball ball = new Ball();
        private GameEntity player = new Player1();
        private AiEnemyPlayer AI = new AiEnemyPlayer();
        private Background bg  = new Background();
        private bool isPause;
        ScreenState gameplay = ScreenState.Play;
        
        //ScoreManager scoreManager = new ScoreManager();
        Keyhandler keyhandler = new Keyhandler();

        public GamePlayManager()
        {
            ball.player = player;
            ball.Ai = AI;
            AI.ball = ball;
            //scoreManager.ball = ball;
            //ball.ScoreManager = scoreManager;
        }


        public ScreenStateManager ScreenStateManager { get; set; } // this was set on the Add method of  ScreenStateManager

        public void Activate(bool b)
        {
            if (b)
            {
                ball.Activate(ScreenStateManager,b);
                player.Activate(ScreenStateManager,b);
                AI.Activate(ScreenStateManager,b);
                bg.Activate(ScreenStateManager,b);
                
                spriteBatch = ScreenStateManager.SpriteBatch;
                //scoreManager.Activate(ScreenStateManager,b);
            }
           
        }

        public void Draw(GameTime gameTime)
        {
            bg.Draw(gameTime);
            ball.Draw(gameTime);
            player.Draw(gameTime);
            AI.Draw(gameTime);
          
           
            //scoreManager.Draw(gameTime);
        }

        public void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            //if (ScreenStateManager.gameplayState == ScreenState.Play ||
            //    ScreenStateManager.gameplayState == ScreenState.Resume)
            //{
            //    if (state.IsKeyDown(Keys.Escape) && oldstate.IsKeyUp(Keys.Escape))
            //    {
            //        Debug.WriteLine("Am i even being called");
            //        ScreenStateManager.gameplayState = ScreenState.Pause;
            //        ScreenStateManager.Add(new PausedScreen());
            //    }
            //}

            /**
                This is definitely a hack since my code is becoming messy
                NOTE: Improve code readability on the next project
            */
            if (state.IsKeyDown(Keys.Escape) && oldstate.IsKeyUp(Keys.Escape))
            {
                ScreenStateManager.escCounter++;
                Debug.WriteLine(ScreenStateManager.escCounter);
                if (ScreenStateManager.escCounter > 2)
                {
                    ScreenStateManager.escCounter = 1;
                }
                if (ScreenStateManager.escCounter == 1)
                {
                    ScreenStateManager.gameplayState = ScreenState.Pause;
                    ScreenStateManager.Add(new PausedScreen());
                }
                
            }
            // this is also a hack. Turns out the code becomes messy when small things were left to be done last.
            if (ScreenStateManager.gameplayState == ScreenState.Quit)
            {
                ScreenStateManager.RemoveScreen(this);
            }
           
           
            if (ball.winner == Winner.Enemy)
            {
                ScreenStateManager.RemoveScreen(this);
                ScreenStateManager.Add(new GameOver("Enemy"));
            }
            else if (ball.winner == Winner.Player)
            {
                ScreenStateManager.RemoveScreen(this);
                ScreenStateManager.Add(new GameOver("Player"));
               
            }
            else
            {
                if (ScreenStateManager.gameplayState == ScreenState.Play || ScreenStateManager.gameplayState == ScreenState.Resume)
                {
                    ball.Update(gameTime, state, oldstate);
                    player.Update(gameTime, state, oldstate);
                    AI.Update(gameTime, state, oldstate);
                }
                
            }
            
            
            
            //scoreManager.Update(gameTime,state,oldstate);
        }
    }
}
