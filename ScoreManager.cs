﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class ScoreManager : GameEntity
    {
        public Ball ball { get; set; }
        private string msg = "";
        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        private SpriteFont scorefont;
        public bool transitionDone;
        public bool enemyScore = false, playerScore = false;
        public override void Activate(ScreenStateManager screenStateManager, bool b)
        {
            base.Activate(screenStateManager, b);
            speed = 500f;
            scorefont = content.Load<SpriteFont>("scorefont");
            


        }

        private void EnemyScore()
        {
          
            if (ball.position.X < 0 && !enemyScore) // do this when the ball position x reach to negative thus scoring the enemy
            {
                position.X = -300;
                speed = 500f;
                direction.X = 1;
                Debug.WriteLine("called again");
                msg = " ENEMY SCORE!";
                enemyScore = true;
            }
            if (position.X > ScreenStateManager.GraphicsDevice.Viewport.Bounds.Width && enemyScore) // lets reset the position when reach the viewport bounds of our screen
            {
                //position.X = -300;
               
                direction.X = 0;   
                transitionDone = true;
                enemyScore = false;
            }
        }

   

        private void PlayerScore()
        {
            if (ball.position.X > ScreenStateManager.GraphicsDevice.Viewport.Bounds.Width && !playerScore )
            {
                position.X = ScreenStateManager.GraphicsDevice.Viewport.Bounds.Width;
                speed = 500f;
                direction.X = -1;
                Debug.WriteLine("called again haha");
                msg = " PLAYER SCORE!";
                playerScore = true;
            }
            if (position.X > -300 && !playerScore) // lets reset the position when reach the left side bounds of our viewport
            {
                
                //position.X = ScreenStateManager.GraphicsDevice.Viewport.Bounds.Width;
                direction.X = 0;
                transitionDone = true;
                playerScore = false;
                
            }
        }

        public override void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {


            EnemyScore();
            PlayerScore();
            
            position += ((direction * (float)gameTime.ElapsedGameTime.TotalSeconds) * speed); ;
        }

        public override void Draw(GameTime gameTime)
        {
            spriteBatch.DrawString(scorefont,msg,position,Color.Red);
        }
    }
}
