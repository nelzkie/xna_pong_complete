﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{

  
    class OptionMenuScreen : IScreenState
    {
        private MenuEntry dificultyEnty;
        List<MenuEntry>menuEntries = new List<MenuEntry>();
        private int selectedEntry = 0;

        private static Level hardship = Level.Easy;
        public OptionMenuScreen()
        {
            dificultyEnty = new MenuEntry(string.Empty);
            dificultyEnty.disablePulsate = true;
            dificultyEnty.SelectInputHandler += DificultyEnty_SelectInputHandler;
            dificultyEnty.CancelSelectInputHandler += OptionExit_SelectInputHandler;
            menuEntries.Add(dificultyEnty);

            setOptionMenuText();
        }

        private void OptionExit_SelectInputHandler(object sender, EventArgs e)
        {
            ScreenStateManager.RemoveScreen(this);
            ScreenStateManager.Add(new MenuScreen());
        }

        private void DificultyEnty_SelectInputHandler(object sender, EventArgs e)
        {
            Debug.WriteLine("keep on calling");
            hardship++;
            if (hardship > Level.Hard)
            {
                hardship = 0;

            }
           ScreenStateManager.dificulty = hardship; // set the dificulty base on the level
            
            setOptionMenuText();
        }

        private void setOptionMenuText()
        {
            dificultyEnty.MSG = " Dificulty: " + hardship;
        }

     
        public ScreenStateManager ScreenStateManager { get; set; }

       

        public void Draw(GameTime gameTime)
        {
            
            for (int i = 0; i < menuEntries.Count; i++)
            {
                menuEntries[i].Position = new Vector2(100,100);
                menuEntries[i].Draw(gameTime, i == selectedEntry,this);
            }
        }

        public void Activate(bool b)
        {
            
        }

        public void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            if (state.IsKeyDown(Keys.Up) && oldstate.IsKeyUp(Keys.Up))
            {
                selectedEntry--;
                if (selectedEntry < 0) selectedEntry = 0;

            }
            if (state.IsKeyDown(Keys.Down) && oldstate.IsKeyUp(Keys.Down))
            {
                selectedEntry++;
                if (selectedEntry > menuEntries.Count - 1) selectedEntry = menuEntries.Count - 1;
            }

            for (int i = 0; i < menuEntries.Count; i++)
            {
                menuEntries[i].Update(gameTime, i == selectedEntry);
            }


            if (state.IsKeyDown(Keys.Right) && oldstate.IsKeyUp(Keys.Right))    //Selection for dificulty
            {

                menuEntries[selectedEntry].OnSelectedEntry();

            }
            if (state.IsKeyDown(Keys.Escape) && oldstate.IsKeyUp(Keys.Escape))  // exit
            {

                menuEntries[selectedEntry].OnCancelSelectedEntry();

            }

        }
    }
}
