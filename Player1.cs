﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace My_Own_Menu_State_System_1
{
    public class Player1 : GameEntity
    {
        Dictionary<Keys,Vector2> keyDictionary = new Dictionary<Keys, Vector2>(); 
        public Player1()
        {
            
            speed = 300f;
            keyDictionary.Add(Keys.Up,new Vector2(0,-1));
            keyDictionary.Add(Keys.Down,new Vector2(0,1));
            
        }

        public override void Activate(ScreenStateManager screenStateManager, bool b)
        {
            base.Activate(screenStateManager, b);
            texture = content.Load<Texture2D>("paddle");
            position = new Vector2(0 + texture.Width / 2, 100);
        }


        public override void Update(GameTime gameTime, KeyboardState state, KeyboardState oldstate)
        {
            Vector2 tmpDirection = Vector2.Zero;
            foreach (var keyValuePair in keyDictionary)
            {
                if (state.IsKeyDown(keyValuePair.Key))
                {
                    if (keyValuePair.Key == Keys.Up)
                    {
                        tmpDirection = keyValuePair.Value;
                    }
                    if (keyValuePair.Key == Keys.Down)
                    {
                       
                        tmpDirection = keyValuePair.Value;
                    }
                }
            }
            direction = tmpDirection;



            base.Update(gameTime, state, oldstate);


        }

        public override void Draw(GameTime gameTime)
        {
            base.Draw(gameTime);
        }
    }
}
